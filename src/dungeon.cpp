#include "dungeon.h"
#include <string>

using namespace std;

Direction noDirection = {.x = 0, .y = 0};

Direction Dungeon::getDirection(string _direction) {
  return directions[_direction];
}

Direction Dungeon::getCoordinates(int x, int y, string _direction) {
  if (moveAllowed(x, y, _direction)) {
    return getDirection(_direction);
  }
  return noDirection;
}

string Dungeon::getRoomName(int xPos, int yPos) {
  return caveMap[yPos][xPos];
}

bool Dungeon::moveAllowed(int xPos, int yPos, string _dir) {
  Direction dir = getDirection(_dir);
  int x = xPos + dir.x;
  int y = yPos + dir.y;
  if (y < 0 || y > sizeof(caveMap)/sizeof(*caveMap) - 1) {
    return false;
  }
  if (x < 0 || x > sizeof(caveMap[yPos])/sizeof(*caveMap[yPos]) - 1) {
    return false;
  }
  return true;
}
