#ifndef PLAYER_H
#define PLAYER_H

#include <string>

#include "dungeon.h"
#include "prompt.h"

using namespace std;

class Player {
  private:
    string name;
    string origin;
    Dungeon dungeon;
    int xPos = 0;
    int yPos = 0;
  public:
    Player(Dungeon _dungeon) {
      name = PROMPT_H::prompt("What is your name?");
      origin = PROMPT_H::prompt("Where are you from?");
      dungeon = _dungeon;
    }
    string getName() { return name; }
    string getOrigin() { return origin; }
    void setDungeon(Dungeon _dungeon) { dungeon = _dungeon; }
    int getXPosition() { return xPos; }
    int getYPosition() { return yPos; }
    void move(string _direction);
};

#endif
