#include "command-map.h"
#include "player.h"
#include <string>

using namespace std;

unordered_map<string, string> init() {
  unordered_map<string, string> umap;

  umap["fight"] = "pow";
  umap["run"] = "eeee";
  umap["shoot"] = "pewpew";
  umap["north"] = "move";
  umap["south"] = "move";
  umap["east"] = "move";
  umap["west"] = "move";

  return umap;
}
