#include "command-handler.h"
#include "command-map.h"
#include "prompt.h"
#include <string>

using namespace std;

void CommandHandler::processCommand(string s, Player* p) {
  string result = commands[s];
  string message = result;
  if (result == "move") {
    message = s + " from " + to_string(p->getXPosition()) + "," + to_string(p->getYPosition());
    p->move(s);
  }
  string response = result.empty() == 1 ? "you want to do what?" : message;
  PROMPT_H::report(response);
}
