#include "dungeon.h"
#include "player.h"
#include <string>

using namespace std;

void Player::move(string _direction) {
  Direction direction = dungeon.getCoordinates(xPos, yPos, _direction);
  xPos += direction.x;
  yPos += direction.y;
}
