#ifndef COMMAND_MAP_H
#define COMMAND_MAP_H
#include <string>

#include <unordered_map>

using namespace std;

unordered_map<string, string> init();

#endif
