#include <iostream>
#include <string>

#include "prompt.h"

using namespace std;

string prompt(string msg) {
  string out;
  cout << "\n" << msg << endl;
  getline(cin, out);
  return out;
}

void reportInline(string out) {
  cout << out;
}

void report(string out) {
  cout << out << endl;
}

string getLine() {
  string s;
  getline(cin, s);
  return s;
}
