#ifndef COMMAND_HANDLER_H
#define COMMAND_HANDLER_H
#include <string>

#include "command-map.h"
#include "player.h"

using namespace std;

class CommandHandler {
  private:
    unordered_map<string, string> commands;
  public:
    CommandHandler() {
      commands = COMMAND_MAP_H::init();
    }
    void processCommand(string s, Player* p);
};

#endif
